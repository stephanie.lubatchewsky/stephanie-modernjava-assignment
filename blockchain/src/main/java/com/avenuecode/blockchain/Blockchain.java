package com.avenuecode.blockchain;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Blockchain {
    private static final String GENESIS_HASH = "000000000000000";
    @Getter
    private List<Block> blockList;

    public Blockchain() {
        this.blockList = new ArrayList<>();
        Block genesis = new Block.BlockBuilder()
                .blockData("Genesis Block")
                .blockPreviousHash(GENESIS_HASH)
                .build();

        blockList.add(genesis);
        log.info(String.format("Blockchain has been initialized! \nGenesis Block: %s",
                blockList.stream()
                        .filter(block -> block.getPrev().equals(GENESIS_HASH))
                        .findAny()
                        .get().getHash()));
    }

    public void addBlock(Block newBlock){
        try {
            blockList.add(newBlock);
        } catch (Exception e){
            log.error(String.format("Cannot add the new block: %s", newBlock.toString()));
        }
    }

    public String getLastHash(){
        return blockList.stream()
                .reduce((first, second) -> second)
                .orElseThrow()
                .getHash();
    }
}
