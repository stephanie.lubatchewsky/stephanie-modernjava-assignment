package com.avenuecode.blockchain;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;


@Slf4j
@Getter
@Setter
public class Block {
    public static final String PREFIX_STRING = "0000";

    private Integer nonce;
    private String data;
    private String prev;
    private String hash;

    public String mineBlock() {
        while (!hash.startsWith(PREFIX_STRING)) {
            nonce++;
            hash = generateBlockHash();
        }
        return hash;
    }

    public String generateBlockHash() {
        var buffer = new StringBuffer();
        var dataToHash = prev + nonce + data;
        byte[] bytes = null;

        try {
            var digest = MessageDigest.getInstance("SHA-256");
            bytes = digest.digest(dataToHash.getBytes("UTF-8"));
            for (byte b : bytes) {
                buffer.append(String.format("%02x", b));
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            log.error(String.valueOf(Level.SEVERE), ex.getMessage());
        }
        return buffer.toString();
    }

    public static class BlockBuilder{
        private String data;
        private String prevHash;

        public BlockBuilder() {
        }

        public BlockBuilder blockData(String data) {
            this.data = data;
            return this;
        }

        public BlockBuilder blockPreviousHash(String prevHash) {
            this.prevHash = prevHash;
            return this;
        }

        public Block build(){
            var block = new Block();
            block.nonce = 0;
            block.data = this.data;
            block.prev = this.prevHash;
            block.hash = block.generateBlockHash();
            block.hash = block.mineBlock();

            return block;
        }
    }

}
