package com.avenuecode.miners;

import com.avenuecode.blockchain.Blockchain;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

@Slf4j
public class Main {
    public static void main(String[] args){
        String input;
        var numMiners = 0;
        var inputValid = false;
        var blockchain = new Blockchain();
        var scanner = new Scanner(System.in);

        while (!inputValid) {
            System.out.println("Please, set the number of miners you wish to start simultaneously: ");
            input = scanner.nextLine();
            try{
                numMiners = Integer.parseInt(input);
                inputValid = true;
            } catch (NumberFormatException e){
                System.out.println("Invalid number, please try again.");
            }
        }

        log.info(String.format("Processing started, %d miners triggered!", numMiners));
        var es = Executors.newFixedThreadPool(numMiners);
        var futures = new ArrayList<Future>();

        IntStream.range(0, numMiners)
                .forEach(
                        i -> futures.add(es.submit(new Miner(blockchain, "thread"+i))));

        futures.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                log.error("Error finishing thread.", e);
            }
        });
        es.shutdown();

        log.info("Blockchain Blocks:");
        blockchain.getBlockList().forEach(
                b -> log.info(String.format(
                        "\n::: Block %s" +
                        "\nNonce: %d" +
                        "\nData: %s " +
                        "\nPrevious Hash: %s :::\n",
                        b.getHash(), b.getNonce(), b.getData(), b.getPrev()))
        );
    }
}
