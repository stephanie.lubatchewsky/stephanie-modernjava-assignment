package com.avenuecode.miners;

import com.avenuecode.blockchain.Block;
import com.avenuecode.blockchain.Blockchain;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Miner implements Runnable{

    private Blockchain blockchain;
    private String threadName;

    public Miner(Blockchain blockchain, String threadName){
        this.blockchain = blockchain;
        this.threadName = threadName;
    }

    @Override
    public void run() {
        Block newBlock;
        boolean blockAdded = false;
        String data = String.format("Block mined by %s", threadName);

        try{
            while(!blockAdded) {
                log.info(String.format("Mining started by %s", threadName));
                String lastHash = blockchain.getLastHash();

                newBlock = createBlock(data, lastHash);
                newBlock.mineBlock();

                if (retrieveLastBlockHash(newBlock.getPrev())) {
                    blockchain.addBlock(newBlock);
                    blockAdded = true;
                    log.info(String.format("A new block has been added by %s with hash %s", threadName, newBlock.getHash()));
                } else {
                    log.info(String.format("Previous hash no longer available while executing %s." +
                            "\n Block hash %s was rejected and will be mined again.", threadName, newBlock.getHash()));
                    Thread.sleep(1000);
                }
            }
        } catch (Exception e) {
            log.error(String.format("Error while mining a new block, %s", threadName), e);
        }
    }

    private Block createBlock(String data, String previousHash){
        return new Block.BlockBuilder()
                .blockPreviousHash(previousHash)
                .blockData(data)
                .build();
    }

    private Boolean retrieveLastBlockHash(String previousHash){
        return previousHash.equals(blockchain.getLastHash());
    }

}
